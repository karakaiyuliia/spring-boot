package com.info5059.casestudy.pdf;

import com.info5059.casestudy.product.Product;
import com.info5059.casestudy.product.ProductRepository;
import com.info5059.casestudy.purchase.PurchaseOrder;
import com.info5059.casestudy.purchase.PurchaseOrderDAO;
import com.info5059.casestudy.purchase.PurchaseOrderLineitem;
import com.info5059.casestudy.vendor.Vendor;
import com.info5059.casestudy.vendor.VendorRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class ReportPDFGenerator extends AbstractPdfView {
    public static ByteArrayInputStream generateReport(String oid,
                                                      PurchaseOrderDAO purchaseOrderDAO,
                                                      VendorRepository vendorRepository,
                                                      ProductRepository productRepository) {
        URL imageUrl = ReportPDFGenerator.class.getResource("/public/images/logo.png");
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Font catFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
        Font subFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Font regular = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
        Locale locale = new Locale("en", "US");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
        try {
            PurchaseOrder report = purchaseOrderDAO.findOne(Long.parseLong(oid));
            PdfWriter.getInstance(document, baos);
            document.open();
            Paragraph preface = new Paragraph();
            // add the logo here
            Image image1 = Image.getInstance(imageUrl);
            image1.setAbsolutePosition(55f, 670f);
            preface.add(image1);
            preface.setAlignment(Element.ALIGN_RIGHT);
            // Lets write a big header
            Paragraph mainHead = new Paragraph(String.format("%55s", "PURCHASE REPORT"), catFont);
            preface.add(mainHead);
            preface.add(new Paragraph(String.format("%90s", "PO#:" + oid), subFont));
            addEmptyLine(preface, 3);
            Optional<Vendor> opt = vendorRepository.findById(report.getVendorid());
            if (opt.isPresent()) {
                Vendor vendor = opt.get();
                PdfPTable head = new PdfPTable(2);
                PdfPCell hCell = new PdfPCell(new Paragraph("Vendor:", smallBold));
                hCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                hCell.setBorder(0);
                head.addCell(hCell);
                hCell = new PdfPCell(new Paragraph(vendor.getName() + "\n" + vendor.getAddress1() + "\n" + vendor.getCity() + "\n" + vendor.getProvince() + "\n" + vendor.getPostalcode(), regular));
                hCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                hCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                hCell.setBorder(0);
                head.addCell(hCell);
                head.setWidthPercentage(50);
                head.setHorizontalAlignment(0);
                preface.add(head);
                addEmptyLine(preface, 3);

                PdfPTable table = new PdfPTable(5);
                PdfPCell cell = new PdfPCell(new Paragraph("Product Code", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Product Description", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Quantity Sold", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Price", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Paragraph("Ext Price", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                BigDecimal tot = new BigDecimal(0.0);
                BigDecimal temp = new BigDecimal(0.0);
                for (PurchaseOrderLineitem line : report.getItems()) {
                    Optional<Product> optx = productRepository.findById(line.getProductid());
                    if ( optx.isPresent() ) {
                        Product product = optx.get();
                        cell = new PdfPCell(new Paragraph(product.getId(), regular));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(product.getName(), regular));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(String.valueOf(line.getQty()), regular));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Paragraph(formatter.format(line.getPrice()), regular));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        tot = line.getPrice().multiply(new BigDecimal(line.getQty()));
                        temp = temp.add(tot);
                        cell = new PdfPCell(new Paragraph(formatter.format(tot), regular));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                    }
                }

                BigDecimal tax = new BigDecimal(0.0);
                // report total
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(0);
                cell.setColspan(3);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Total:"));
                cell.setBorder(0);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(formatter.format(temp)));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                tax = temp.multiply(new BigDecimal(0.13));
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(0);
                cell.setColspan(3);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Tax:"));
                cell.setBorder(0);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(formatter.format(tax)));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);

                tax = temp.multiply(new BigDecimal(0.13));
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(0);
                cell.setColspan(3);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Order Total:"));
                cell.setBorder(0);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(formatter.format(tax.add(temp))));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBackgroundColor(BaseColor.YELLOW);
                table.addCell(cell);
                
                preface.add(table);
            }
            addEmptyLine(preface, 3);

            preface.setAlignment(Element.ALIGN_CENTER);
            preface.add(new Paragraph(String.format("%60s", new Date()), subFont));

            document.add(preface);
            document.close();
        } catch (Exception ex) {
            Logger.getLogger(ReportPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
