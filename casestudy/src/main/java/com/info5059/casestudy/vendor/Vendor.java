package com.info5059.casestudy.vendor;
/**
* Program Name: Vendor
* Purpose:
* Coder: Yuliia Karakai
* Date: 2019-09-10
*/

import com.info5059.casestudy.product.Product;
import lombok.Data;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Vendor {
    @OneToMany(mappedBy = "vendorid", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Product> products = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private String name;
    private String address1;
    private String city;
    private String province;
    private String postalcode;
    private String phone;
    private String type;
    private String email;

    public long getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public String getAddress1() {
        return address1;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getPhone() {
        return phone;
    }

    public String getType() {
        return type;
    }

    public String getEmail() {
        return email;
    }
}
