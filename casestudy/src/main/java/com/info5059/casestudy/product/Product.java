package com.info5059.casestudy.product;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.math.BigDecimal;

@Entity
@Data
public class Product {
    @Id
    private String Id;
    private int vendorid;
    private String name;
    private BigDecimal costprice;
    private BigDecimal msrp;
    private int rop;
    private int eoq;
    private int qoh;
    private int qoo;
    @Lob
    @Basic(optional = true)
    private byte[] qrcode;
    private String qrcodetxt;

    public String getId() {
        return Id;
    }

    public int getVendorid() {
        return vendorid;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getCostprice() {
        return costprice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public int getRop() {
        return rop;
    }

    public int getEoq() {
        return eoq;
    }

    public int getQoh() {
        return qoh;
    }

    public int getQoo() {
        return qoo;
    }

    public byte[] getQrcode() {
        return qrcode;
    }

    public String getQrcodetxt() {
        return qrcodetxt;
    }

    public void setCostprice(BigDecimal costprice) {
        this.costprice = costprice;
    }

    public void setEoq(int eoq) {
        this.eoq = eoq;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public void setQoo(int qoh) {
        this.qoh = qoh;
    }

    public void setRop(int rop) {
        this.rop = rop;
    }

    public void setQrcodetxt(String qrcodetxt) {
        this.qrcodetxt = qrcodetxt;
    }

    public void setQrcode(byte[] generateQRCode) {
        this.qrcode = generateQRCode;
    }
}
