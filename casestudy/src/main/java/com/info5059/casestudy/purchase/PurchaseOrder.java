package com.info5059.casestudy.purchase;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class PurchaseOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private Long vendorid;
    private BigDecimal amount;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date podate;
    @OneToMany(mappedBy = "poid", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PurchaseOrderLineitem> items = new ArrayList<PurchaseOrderLineitem>();

    public void setId(Long id) {
        Id = id;
    }

    public void setVendorid(Long vendorid) {
        this.vendorid = vendorid;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setPodate(Date podate) {
        this.podate = podate;
    }

    public void setItems(List<PurchaseOrderLineitem> items) {
        this.items = items;
    }

    public Long getId() {
        return Id;
    }

    public Long getVendorid() {
        return vendorid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getPodate() {
        return podate;
    }

    public List<PurchaseOrderLineitem> getItems() {
        return items;
    }

}
