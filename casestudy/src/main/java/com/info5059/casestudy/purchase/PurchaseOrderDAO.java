package com.info5059.casestudy.purchase;

import com.info5059.casestudy.product.Product;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.Date;
@Component
public class PurchaseOrderDAO {
    @PersistenceContext
    private EntityManager entityManager;
    @Transactional
    public Long create(PurchaseOrder po) {
        PurchaseOrder realpo = new PurchaseOrder();
        realpo.setPodate(new Date());
        realpo.setVendorid(po.getVendorid());
        realpo.setAmount(po.getAmount());
        entityManager.persist(realpo);
        for(PurchaseOrderLineitem item :po.getItems()) {
            PurchaseOrderLineitem realItem = new PurchaseOrderLineitem();
            realItem.setPoid(realpo.getId());
            realItem.setProductid(item.getProductid());
            realItem.setQty(item.getQty());
            realItem.setPrice(item.getPrice());
            entityManager.persist(realItem);
        }
        return realpo.getId();
    }

    public PurchaseOrder findOne(Long id) {
        PurchaseOrder po = entityManager.find(PurchaseOrder.class, id);
        if (po == null) {
            throw new EntityNotFoundException("Can't find order for ID "
                    + id);
        }
        return po;
    }
}
