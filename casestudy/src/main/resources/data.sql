INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email)VALUES ('123 Maple
St','London','On', 'N1N-1N1','(555)555-5555','Trusted','ABC Supply Co.','abc@supply.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email) VALUES ('543
Sycamore Ave','Toronto','On', 'N1P-1N1','(999)555-5555','Trusted','Big Bills
Depot','bb@depot.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email) VALUES ('922 Oak
St','London','On', 'N1N-1N1','(555)555-5599','Un Trusted','Shady Sams','ss@underthetable.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email) VALUES ('678 Test
St','London','On', 'N1N-1N1','(234)545-8291','Un Trusted','Yuliia Karakai','yk@underthetable.com');
--products
INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('12x45',4,'Pixel phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('13x45',4,'Apple phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('14x45',1,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('14x46',1,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('14x47',1,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('14x48',1,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('15x45',1,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('16x45',3,'Pixel phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('17x45',2,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('18x45',3,'OnePlus phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('19x45',4,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('20x45',2,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('21x45',2,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('22x45',2,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);
 INSERT INTO Product (Id,vendorid,name,costprice,msrp,rop,eoq,qoh,qoo,qrcode,qrcodetxt)
 VALUES ('23x45',2,'Samsung phone',1239.99,1239.99,120,200,300,400,null,null);

 -- sample report
INSERT INTO Purchase_Order (VendorId,PoDate) VALUES (1, CURRENT_TIMESTAMP);
INSERT INTO Purchase_Order_LineItem (PRODUCTID,POID, QTY, PRICE) VALUES ('22x45',1,10, 299.99);
INSERT INTO Purchase_Order (VendorId,PoDate) VALUES (2, CURRENT_TIMESTAMP);
INSERT INTO Purchase_Order_LineItem (PRODUCTID,POID, QTY, PRICE) VALUES ('14x48',2,10, 399.99);
INSERT INTO Purchase_Order (VendorId,PoDate) VALUES (1, CURRENT_TIMESTAMP);
INSERT INTO Purchase_Order_LineItem (PRODUCTID,POID, QTY, PRICE) VALUES ('12x45',1,5, 100);
