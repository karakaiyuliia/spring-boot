export interface Vendor {
  id: number;
  type: string;
  name: string;
  phone: string;
  address1: string;
  postalcode: string;
  email: string;
  city: string;
  province: string;
}

