/**
 * ReportItem - container class for expense report item
 */
export interface ReportItem {
  id: number;
  reportid: number;
  productid: string;
  total: number;
  price: number;
  qty: number;
  name: string;
}
