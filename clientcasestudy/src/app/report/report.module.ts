import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportGeneratorComponent } from './generator/report-generator.component';
import {MatCardModule, MatFormFieldModule, MatToolbarModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import { ReportViewerComponent } from './viewer/report-viewer.component';


@NgModule({
  declarations: [ReportGeneratorComponent, ReportViewerComponent],
  imports: [
    CommonModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatToolbarModule
  ]
})
export class ReportModule { }
