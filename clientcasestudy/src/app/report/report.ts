import { ReportItem } from './report-item';
/**
 * Report - interface for expense report
 */
export interface Report {
  id: number;
  vendorid: number;
  items: ReportItem[];
  amount: number;
  podate: string;
}
