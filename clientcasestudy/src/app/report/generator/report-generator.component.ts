import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Vendor } from '../../vendor/vendor';
import { RestfulService } from '../../restful.service';
import { Product } from '../../product/product';
import { ReportItem } from '../report-item';
import { Report } from '../report';
import { BASEURL, PDFURL } from '../../constants';
@Component({
  templateUrl: './report-generator.component.html'
})
export class ReportGeneratorComponent implements OnInit {
  // form
  generatorForm: FormGroup;
  vendorid: FormControl;
  productid: FormControl;
  qty: FormControl;
  // component
  products: Array<Product>;
  vendors: Array<Vendor>;
  items: Array<ReportItem>;
  qtys: Array<number>;
  selectedproducts: Array<Product>;
  vendorproducts: Array<Product>;
  selectedProduct: Product;
  selectedVendor: Vendor;
  pickedProduct: boolean;
  pickedVendor: boolean;
  generated: boolean;
  hasProducts: boolean;
  msg: string;
  sub: number;
  tax: number;
  final: number;
  reportno: number;
  url: string;
  constructor(private builder: FormBuilder, private restService: RestfulService) {
    this.pickedVendor = false;
    this.pickedProduct = false;
    this.generated = false;
    this.url = BASEURL + 'api/pos';
  } // constructor
  ngOnInit() {
    this.msg = '';
    this.vendorid = new FormControl('');
    this.productid = new FormControl('');
    this.qty = new FormControl('');
    this.qtys = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.generatorForm = this.builder.group({
      productid: this.productid,
      vendorid: this.vendorid,
      qty: this.qty
    });
    this.onPickVendor();
    this.onPickProduct();
    this.onPickEOQ();
    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      vendorPayload => {
        this.vendors = vendorPayload._embedded.vendors;
        this.msg = 'vendors loaded';
        this.msg = 'loading products from server...';
        this.restService.load(BASEURL + 'products').subscribe(
          productPayload => {
            this.products = productPayload._embedded.products;
            this.msg = 'server data loaded';
          },
          err => {this.msg += `Error occurred - products not loaded - ${err.status} - ${err.statusText}`;
          });
      },
      err => {this.msg += ` Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      });
  } // ngOnInit

  /**
   * viewPdf - determine report number and pass to server
   * for PDF generation in a new window
   */
  viewPdf() {
    window.open(PDFURL + this.reportno, '');
  } // viewPdf
  /**
   * onPickEmployee - subscribe to the select change event then load specific
   * employee expenses for subsequent selection
   */
  onPickVendor(): void {
    this.generatorForm.get('vendorid').valueChanges.subscribe(val => {
      this.selectedProduct = null;
      this.selectedVendor = val;
      this.loadVendorProducts();
      this.pickedProduct = false;
      this.hasProducts = false;
      this.msg = 'choose product for vendor';
      this.pickedVendor = true;
      this.generated = false;
      this.items = [];
      this.selectedproducts = [];
    });
  }
  onPickEOQ(): void {
    this.generatorForm.get('qty').valueChanges.subscribe(val => {
      const item: ReportItem = {id: 0, name: '', reportid: 0, productid: this.selectedProduct.id,
        total: val * this.selectedProduct.costprice, price: this.selectedProduct.costprice, qty: val};

      const it = this.items.find(i => i.productid === this.selectedProduct.id);
      if (it) {
        this.items.splice(this.items.indexOf(it), 1);
      }

      if (val === 0) {
        this.msg = 'Item deleted';
      } else {
        this.items.push(item);
        this.selectedproducts.push(this.selectedProduct);
      }

      this.sub = 0.0;
      this.tax = 0.0;
      this.final = 0.0;
      this.items.forEach(s => this.sub += s.total);
      this.tax = this.sub * 0.13;
      this.final = this.sub + this.tax;

    });
  }
  /**
   * onPickProducts - subscribe to the select change event then
   * update array containing items.
   */
  onPickProduct(): void {
    this.generatorForm.get('productid').valueChanges.subscribe(val => {
      this.pickedProduct = true;
      this.selectedProduct = val;

      this.hasProducts = true;

    });
  }
  /**
   * loadVendorProducts - filter for a particular vendor's products
   */
  loadVendorProducts() {
    this.vendorproducts = [];
    this.vendorproducts = this.products.filter(pr => pr.vendorid === this.selectedVendor.id);
  }
  /**
   * createReport - create the client side report
   */
  createReport() {
    this.generated = false;
    const report: Report = {id: 0, items: this.items, vendorid: this.selectedProduct.vendorid, amount: this.final, podate: null};
    console.log(report);
    this.restService.add(this.url, report).subscribe(
      reportId => {
        if (reportId > 0) { // server returns new report#
          this.msg = `Product Report ${reportId} created!`;
          this.generated = true;
          this.reportno = reportId;
        } else {
          this.msg = 'Report not created! - server error';
        }
        this.hasProducts = false;
        this.pickedVendor = false;
        this.pickedProduct = false;
      },
      err => {
        this.msg = `Error occurred - report not created - ${err.status} - ${err.statusText}`;
      }
    );
  } // createReport
} // ReportGeneratorComponent
