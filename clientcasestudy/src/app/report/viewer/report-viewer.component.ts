import {Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Vendor } from '../../vendor/vendor';
import { RestfulService } from '../../restful.service';
import { Product } from '../../product/product';
import { ReportItem } from '../report-item';
import { Report } from '../report';
import { BASEURL, PDFURL } from '../../constants';

@Component({
  templateUrl: './report-viewer.component.html'
})
export class ReportViewerComponent implements OnInit {
  // form
  viewerForm: FormGroup;
  vendorid: FormControl;
  reportid: FormControl;
  // component
  reports: Array<Report>;
  vendors: Array<Vendor>;
  items: Array<ReportItem>;
  selectedreport: Array<Report>;
  vendorreports: Array<Report>;
  selectedproducts: Array<ReportItem>;
  allproducts: Array<Product>;
  selectedReport: Report;
  selectedVendor: Vendor;
  pickedReport: boolean;
  pickedVendor: boolean;
  generated: boolean;
  hasReports: boolean;
  msg: string;
  url: string;
  sub: number;
  tax: number;
  final: number;
  constructor(private builder: FormBuilder, private restService: RestfulService) {
    this.pickedVendor = false;
    this.pickedReport = false;
    this.generated = false;
    this.url = BASEURL + 'viewers';
  } // constructor
  ngOnInit() {
    this.msg = '';
    this.vendorid = new FormControl('');
    this.reportid = new FormControl('');
    this.viewerForm = this.builder.group({
      reportid: this.reportid,
      vendorid: this.vendorid
    });
    this.onPickVendor();
    this.onPickReport();
    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      vendorPayload => {
        this.vendors = vendorPayload._embedded.vendors;
        this.msg = 'vendors loaded';
        this.msg = 'loading product from server...';
        this.restService.load(BASEURL + 'pos').subscribe(
          reportPayload => {
            this.reports = reportPayload._embedded.pos;
            this.msg = 'server data loaded';
          },
          err => {this.msg += `Error occurred - reports not loaded - ${err.status} - ${err.statusText}`;
          });
      },
      err => {this.msg += ` Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      });
  }

  viewPdf() {
    window.open(PDFURL + this.selectedReport.id, '');
  } // viewPdf

  /**
   * onPickVendor - subscribe to the select change event then load specific
   * employee expenses for subsequent selection
   */
  onPickVendor(): void {
    this.viewerForm.get('vendorid').valueChanges.subscribe(val => {
      this.selectedReport = null;
      this.selectedVendor = val;
      this.loadVendorReports();
      this.loadProducts();
      this.pickedReport = false;
      this.hasReports = false;
      this.msg = 'choose report for vendor';
      this.pickedVendor = true;
      this.generated = false;
      this.items = [];
      this.selectedreport = [];
    });
  }

  /**
   * onPickReport - subscribe to the select change event then
   * update array containing items.
   */
  onPickReport(): void {
    this.viewerForm.get('reportid').valueChanges.subscribe(val => {
      this.selectedReport = val;
      this.hasReports = true;
      this.loadReportDetails();
      //this.extended = 0.0;
      //this.selectedproducts.forEach(exp => this.extended += exp.price * exp.qty);
    });
  }

  /**
   * loadVendorReports - filter for a particular employee's reports
   */
  loadVendorReports() {
    this.vendorreports = [];
    this.vendorreports = this.reports.filter(ex => ex.vendorid === this.selectedVendor.id); // filter reports for single vendor
  }

  loadProducts() {
    this.allproducts = [];
    this.restService.load(BASEURL + 'products').subscribe(
      payload => {
        this.allproducts = payload._embedded.products;
      },
      err => {this.msg += `Error occurred - products not loaded - ${err.status} - ${err.statusText}`;
      });
  }

  loadReportDetails() {
    this.selectedproducts = [];
    this.sub = 0.0;

    this.selectedReport.items.forEach(item => {
      const p: Product =  ( this.allproducts.find( pr => item.productid === pr.id));
      item.name = p.name;
      this.selectedproducts.push(item);
      this.sub += item.qty * item.price;
    });

    this.tax = 0.0;
    this.final = 0.0;
    this.tax = this.sub * 0.13;
    this.final = this.sub + this.tax;

  }
}
