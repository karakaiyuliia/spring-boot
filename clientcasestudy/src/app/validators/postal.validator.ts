import { AbstractControl } from '@angular/forms';
export function ValidatePostal(control: AbstractControl) {
  const PHONE_REGEXP = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
  return !PHONE_REGEXP.test(control.value) ? {invalidPostal: true} : null;
} // ValidatePhone
