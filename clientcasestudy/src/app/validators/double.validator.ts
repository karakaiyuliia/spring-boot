import { AbstractControl } from '@angular/forms';
export function ValidateDouble(control: AbstractControl) {
  const DOUBLE_REGEXP = /^\d+\.\d{2}$/;
  return !DOUBLE_REGEXP.test(control.value) ? {invalidDouble: true} : null;
} // ValidateDouble
