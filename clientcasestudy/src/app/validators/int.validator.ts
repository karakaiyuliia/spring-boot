import { AbstractControl } from '@angular/forms';
export function ValidateInt(control: AbstractControl) {
  const INT_REGEXP = /^-?[0-9][^\.]*$/;
  return !INT_REGEXP.test(control.value) ? {invalidInt: true} : null;
} // ValidatePhone
